// Use the "require" directive to load the express module/package
// It also allows us access to methods and functions that will allow us to easily create a server
const express = require("express");

// This creates an express application and stores this in a constant called app
// In layman's terms, app is our server
const app = express();

// For our application server to run, we need a port to listen to
const port = 3000;

// Middlewares - software that provides common services and capabilities to applications outside of what's offered by the operating system

// allows your app to read JSON data
app.use(express.json());

// Allows your app to read data from forms
// By applying the option of "extended:true" this allows us to receive information in other data types such as an object which we will use throughout our application
app.use(express.urlencoded({extended: true}));


// [SECTION] ROUTES
// GET Method
app.get("/greet", (request, response) => {
	response.send("Hello from the /greet endpoint!");
});

// POST Method
// This route expects to receive a POST request at the URI "/hello"
app.post("/hello", (request, response) => {
	// req.body contains the contents/data of the request body
	// All the properties defined in our Postman request will be accessible here as properties with the same names
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});


// Simple registration form

// An array that will store user objects when the "/signup" route is accessed
// This will serve as our mock database
let users = [];

app.post("/signup", (request, response) => {

	// If contents of the "request body" with the property "username" and "password" is not empty
	if( request.body.username !== '' && request.body.password !== '' ) {
		// This will store the user object sent via Postman to the users array created above
		users.push(request.body);
		// This will send a response back to the client/Postman after the request has been processed
		response.send(`User ${request.body.username} successfully registered!`);
	} else {
		// If the username and password are not complete an error message will be sent back to the client/Postman
		response.send("Please input BOTH username and password.");
	}
});


// Simple change password transaction

app.patch("/change-password", (request, response) => {

	// Creates a variable to store the message to be sent back to the client/Postman 
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){
		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(request.body.username == users[i].username){
			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = request.body.password;
			// Changes the message to be sent back by the response
			message = `User ${request.body.username}'s password has been updated!`;

			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found
			break;

		// If no user was found
		} else {
			// Changes the message to be sent back by the response
			message = "User does not exist."
		}
	}
	// Sends a response back to the client/Postman once the password has been updated or if a user is not found
	response.send(message);
});


// Tells our server to listen to the port
// If the port is accessed, we can run the server
// Returns a message to confirm that the server is running in the terminal


// [SECTION] Activity
app.get("/home", (request, response) => {
	response.send("Welcome to the homepage!");
});


app.get("/users", (request, response) => {
	response.send(
		[
			{
				"username": "johndoe",
				"password": "johndoe1234"
			}
		])
});

app.delete("/delete-user", (request,response) => {
	response.send(`User ${request.body.username} has been deleted`);

})


app.listen(port, () => console.log(`Server running at ${port}`));




